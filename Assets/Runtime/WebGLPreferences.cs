﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
[ExecuteInEditMode]
public class WebGLPreferences
{
    static WebGLPreferences _current;

    [SerializeField] string _language;
    [SerializeField] KeyCode[] _inputKeys;
    [SerializeField] float _reactionTime = 5f;
    [SerializeField] int _volumeLevel = 1;
    [SerializeField] float _sessionTime;

    [NonSerialized] float _lastUpdateTime = 0;

    public static WebGLPreferences Current
    {
        get
        {
            if (_current != null)
            {
                return _current;
            }

            Load();
            return _current;
        }
        set
        {
            _current = value;
        }
    }

    public static void Load() { _current = PreferenceManager.Load<WebGLPreferences>(); }

    public KeyCode[] InputKeys { get => _inputKeys; set { _inputKeys = value; Save(); } }
    public bool HasKeys => InputKeys != null && InputKeys.Length > 1;

    public KeyCode PrimaryKey => HasKeys ? InputKeys[0] : KeyCode.None;
    public KeyCode SecondaryKey => HasKeys ? InputKeys[1] : KeyCode.None;

    public bool GetPrimaryKeyDown => HasKeys && Input.GetKeyDown(PrimaryKey);
    public bool GetSecondaryKeyDown => HasKeys && Input.GetKeyDown(SecondaryKey);


    public float ReactionTime { get => _reactionTime; set { _reactionTime = value; Save(); } }
    public int VolumeLevel { get => _volumeLevel; set { _volumeLevel = value; Save(); } }
    public string Language { get => _language; set { _language = value; Save(); } }

    public float SessionTime { get => _sessionTime; set { _sessionTime = value; Save(); } }

    public void UpdateSessionTime()
    {
        if (_lastUpdateTime == 0) SessionTime = Time.unscaledTime;
        else SessionTime += Time.unscaledTime - _lastUpdateTime;
        _lastUpdateTime = SessionTime;
        Save();
    }


    public static void Save()
    {
        PreferenceManager.Save(_current);
        WebGLData.Save();
    }

    public override string ToString()
    {
        var returnString = "WebGL Preferences: ";
        returnString += JsonUtility.ToJson(this);
        return returnString;
    }
}
