﻿using System.IO;
using UnityEngine;

public class WebGLRedirect
{
    static bool isRedirecting = false;

    private static void Redirect(string url)
    {
        if (isRedirecting) return;
        isRedirecting = true;

        var preferences = JsonUtility.ToJson(WebGLPreferences.Current);
#if UNITY_WEBGL && !UNITY_EDITOR
        if (preferences != "{}") 
        {
            JSLib.RedirectWithParams(url, preferences);
        }
        else 
        {
            JSLib.Redirect(url);
        }
#endif
        Debug.LogWarning($"Should open an encoded version of '{url}?{JsonUtility.ToJson(WebGLPreferences.Current)}' now");
    }

    public static void RefreshPage()
    {
        Redirect("./");
    }

    public static void GoBackOnePage()
    {
        Redirect("../");
    }

    public static void OpenGame(string relativeGameUrl)
    {
        Redirect("./" + relativeGameUrl + "/");
    }
}