﻿public class WebGLParameters
{
    public static string GetParameterJson()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        return JSLib.GetParams();
#endif
        return "";
    }
}
